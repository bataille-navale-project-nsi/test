d1 = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'a1': 5, 'b1': 10, 'c1': 8, 'd1': 6}
for k, v in sorted(d1.items(), key=lambda x: x[1]):
    print("%s: %s" % (k, v))
