# On importe une variable d'une fonction qui va permettre
# de rendre aleatoire le choix des cases.
from random import randint
liste_nom = []
# On stocke les données (noms des personnages) dans un tableau
# contenantants des dictionnaires.
with open("Characters.csv", mode='r', encoding='utf-8') as f:
    lines = f.readlines()
    key_line = lines[0].strip()
    keys = key_line.split(";")
    for line in lines[1:]:
        line = line.strip()
        values = line.split(';')
        dico_persos = {}
        for i in range(len(keys)):
            if keys[i] == 'Name':
                dico_persos[keys[i]] = values[i]
        liste_nom.append(dico_persos)
# On crée un dictionnaire vide afin des trier les personnages
# pour plus tard et de faire la moyenne.
tri = {}
# On fait un programme pour que chaque joueur puisse jouer 10
# fois chacun avec 3 essais.
for perso in liste_nom:
    points_total = 0
    points = 0
    for i in range(10):
        points = 0
# On va créer des tableaux dans un tableau correspondant aux differentes cases.
        case = [['a1', 'a2', 'a3', 'a4', 'a5'],
                ['b1', 'b2', 'b3', 'b4', 'b5'],
                ['c1', 'c2', 'c3', 'c4', 'c5'],
                ['d1', 'd2', 'd3', 'd4', 'd5'],
                ['e1', 'e2', 'e3', 'e4', 'e5']]
        case_navire_1 = case[randint(0,  4)][randint(0, 4)]
        case_navire_2 = case[randint(0, 4)][randint(0, 4)]
        case_precedente = [0, 0, 0]
        while case_navire_1 == case_navire_2:
            case_navire_2 = case[randint(0, 4)][randint(0, 4)]
        for essai in range(3):
            if points < 16:
                case_touche = case[randint(0, 4)][randint(0, 4)]
                while case_touche == case_precedente[0] or (
                    case_touche == case_precedente[1]):
                    case_touche = case[randint(0, 4)][randint(0, 4)]
                if case_touche == case_navire_1:
                    points += 8
                elif case_touche[0] == case_navire_1[0]:
                    points += 1
                elif case_touche[1] == case_navire_1[1]:
                    points += 1
                if case_touche == case_navire_2:
                    points += 8
                elif case_touche[0] == case_navire_2[0]:
                    points += 1
                elif case_touche[1] == case_navire_2[1]:
                    points += 1
                case_precedente[essai] = case_touche
                if points > 16:
                    points = 16
                points_total += points
# On attribue le nom du perso en clé et les points totaux en valeur
# afin de le trier dans l'ordre décroissant les valeurs (scores).
    tri[perso['Name']] = points_total
for k, v in sorted(tri.items(), reverse=True, key=lambda x: x[1]):
    print("Le joueur %s a %s points." % (k, v))
# Fonction permettant de faire la moyenne des valeurs.
moyenne = sum(tri.values()) / (len(tri) + 1)
print(f"La moyenne est de {moyenne}.")
