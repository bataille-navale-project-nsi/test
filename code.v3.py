# -*- coding: utf-8 -*-
"""
Created on Fri Nov 19 11:12:59 2021

@author: RAVILY
"""
from random import randint

liste_nom = ('coucou','test', 'oui', 'non')
for perso in liste_nom :
        
    points = 0
    
    case = [['a1', 'a2', 'a3', 'a4', 'a5'],
            ['b1', 'b2', 'b3', 'b4', 'b5'],
            ['c1', 'c2', 'c3', 'c4', 'c5'],
            ['d1', 'd2', 'd3', 'd4', 'd5'],
            ['e1', 'e2', 'e3', 'e4', 'e5']]

    case_navire_1 = case[randint(0,  4)] [randint(0, 4)]
    case_navire_2 = case[randint(0, 4)] [randint(0, 4)]
    case_precedente = [0, 0, 0]
    while case_navire_1 == case_navire_2 :
        case_navire_2 = case[randint(0, 4)] [randint(0, 4)]
    for essai in range(3) :
        if points < 16 :
            case_touche = case[randint(0, 4)] [randint(0, 4)]
            while case_touche == case_precedente[0] or (
                case_touche == case_precedente[1] ) :
                case_touche = case[randint(0, 4)] [randint(0, 4)]
            #print(f"Résultat essai {essai-1}:  {case_touche}")
            if case_touche == case_navire_1 :
                points += 8
            elif case_touche[0] == case_navire_1[0] :
                points += 1
            elif case_touche[1] == case_navire_1[1] :
                points += 1
            if case_touche == case_navire_2 :
                points += 8
            elif case_touche[0] == case_navire_2[0] :
                points += 1
            elif case_touche[1] == case_navire_2[1] :
                points += 1
            case_precedente[essai] = case_touche
            print(points)

    print(f"Position du bateau 1 : {case_navire_1},  Position du bateau 2 : {case_navire_2}")
    print(f"Cases choisies précédemment : {case_precedente}")
    print(f"{perso}: {points:} points.")

